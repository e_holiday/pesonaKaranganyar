package com.gits.developer.pesonakaranganyar.ui.nomorsos;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.gits.developer.pesonakaranganyar.R;
import com.gits.developer.pesonakaranganyar.databinding.ActivityNomorSosBinding;
import com.gits.developer.pesonakaranganyar.model.nomorsos.NomorSOSModel;
import com.gits.developer.pesonakaranganyar.model.nomorsos.SOSContactData;
import com.gits.developer.pesonakaranganyar.model.nomorsos.SOSResponseServer;
import com.gits.developer.pesonakaranganyar.network.config.MyConn;
import com.gits.developer.pesonakaranganyar.network.service.NomorSOSService;
import com.gits.developer.pesonakaranganyar.ui.nomorsos.adapter.NomorSOSRVadapter;
import com.gits.developer.pesonakaranganyar.util.PDialog;
import com.gits.developer.pesonakaranganyar.util.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kazt on 20/06/17.
 */

public class NomorSOSFragment extends Fragment {
    private static final int REQUEST_CODE_CALL = 28;
    private ActivityNomorSosBinding binding;
    private List<NomorSOSModel> nomorSOSModels = new ArrayList<>();
    private RecyclerView.LayoutManager layoutManager;
    private NomorSOSRVadapter nomorSOSRVadapter;
    private PDialog pDialog;
    private MyConn myConn = new MyConn();
    private NomorSOSService service;
    private List<SOSContactData> datas = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.activity_nomor_sos, container, false);
        pDialog = new PDialog(getActivity());
        getData();
        return binding.getRoot();
    }

    private void getData() {
        service = myConn.createService(NomorSOSService.class);
        Call<SOSResponseServer> call = service.getNomorSOS();
        pDialog.showPDialog();
        call.enqueue(new Callback<SOSResponseServer>() {
            @Override
            public void onResponse(Call<SOSResponseServer> call, Response<SOSResponseServer> response) {
                pDialog.hidePDialog();
                if (response.body().isStatus()){
                    datas = response.body().getDatas();
                    setRV();
                }else {
                    Toast.makeText(getActivity(), "Nomor kontak belum tersedia", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SOSResponseServer> call, Throwable t) {
                pDialog.hidePDialog();
            }
        });
    }

    private void setRV() {
        layoutManager = new LinearLayoutManager(getActivity());
        nomorSOSRVadapter = new NomorSOSRVadapter(getActivity(), datas);
        binding.rvNomorSos.setLayoutManager(layoutManager);
        binding.rvNomorSos.setAdapter(nomorSOSRVadapter);
        binding.rvNomorSos.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),(view, position) -> {
            callSOS(datas.get(position).getNumber());
        }));
    }

    private void callSOS(String number) {
        if(ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED){
//            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + number.trim()));
//            startActivity(intent);
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:"+number.trim()));
            startActivity(callIntent);
        }else{
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CODE_CALL);
        }
    }


}
