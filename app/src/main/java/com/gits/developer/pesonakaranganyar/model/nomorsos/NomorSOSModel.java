package com.gits.developer.pesonakaranganyar.model.nomorsos;

import java.io.Serializable;

/**
 * Created by kazt on 20/06/17.
 */

public class NomorSOSModel implements Serializable {
    private int idNomor;
    private String phoneName;
    private String phoneNumber;
    private String phoneDesc;

    public int getIdNomor() {
        return idNomor;
    }

    public void setIdNomor(int idNomor) {
        this.idNomor = idNomor;
    }

    public String getPhoneName() {
        return phoneName;
    }

    public void setPhoneName(String phoneName) {
        this.phoneName = phoneName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneDesc() {
        return phoneDesc;
    }

    public void setPhoneDesc(String phoneDesc) {
        this.phoneDesc = phoneDesc;
    }
}
