package com.gits.developer.pesonakaranganyar.model.nomorsos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by kazt on 10/07/17.
 */

public class SOSResponseServer implements Serializable{
    @SerializedName("status")
    @Expose
    private boolean status;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    @Expose
    private List<SOSContactData> datas = null;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SOSContactData> getDatas() {
        return datas;
    }

    public void setDatas(List<SOSContactData> datas) {
        this.datas = datas;
    }
}
