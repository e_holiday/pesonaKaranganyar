package com.gits.developer.pesonakaranganyar.ui.nomorsos.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.gits.developer.pesonakaranganyar.databinding.ItemNomorSosBinding;
import com.gits.developer.pesonakaranganyar.model.nomorsos.SOSContactData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kazt on 20/06/17.
 */

public class NomorSOSRVadapter extends RecyclerView.Adapter<NomorSOSRVadapter.sosViewHolder> {
    private Context c;
    private List<SOSContactData> datas = new ArrayList<>();
    private ItemNomorSosBinding binding;

    public NomorSOSRVadapter(Context context, List<SOSContactData> datas1) {
        this.c = context;
        this.datas = datas1;
    }

    @Override
    public sosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        binding = ItemNomorSosBinding.inflate(LayoutInflater.from(c),parent,false);
        return new sosViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(sosViewHolder holder, int position) {
        SOSContactData data = datas.get(position);
        holder.bind(data);
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public class sosViewHolder extends RecyclerView.ViewHolder{
        private final ItemNomorSosBinding binding;

        public sosViewHolder(ItemNomorSosBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(SOSContactData sosContactData){
            binding.setNomorSOSModel(sosContactData);
            binding.executePendingBindings();
        }
    }
}
