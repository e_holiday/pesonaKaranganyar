package com.gits.developer.pesonakaranganyar.network.service;

import com.gits.developer.pesonakaranganyar.model.nomorsos.SOSResponseServer;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by kazt on 10/07/17.
 */

public interface NomorSOSService {

    @GET("/api/eholiday/getnomorsos")
    Call<SOSResponseServer> getNomorSOS();

}
